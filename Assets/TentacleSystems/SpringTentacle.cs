using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;

namespace TentacleSystems
{
    public class SpringTentacle : MonoBehaviour, INodePositionProvider
    {
        [MinValue(1)][BoxGroup("Node Setup")]
        public int nodeCount;
        [BoxGroup("Node Setup")]
        public float nodeMass;
        [BoxGroup("Node Setup")]
        public float segmentLength;
        
        [BoxGroup("Spring")]
        public bool simulateSprings;
        [ShowIf(nameof(simulateSprings)), BoxGroup("Spring")]
        public float k;
        [ShowIf(nameof(simulateSprings)), BoxGroup("Spring")]
        public float dampening;

        
        [FormerlySerializedAs("constrainLength")] [BoxGroup("Constraints")]
        public bool constrainMaxLength;
        [BoxGroup("Constraints")]
        [ShowIf(nameof(constrainMaxLength)), ValidateInput(nameof(CheckLengthConstrainDistance), "Must be above segmentLength")]
        public float maxLengthConstrainDistance;
        [BoxGroup("Constraints")]
        [ShowIf(nameof(constrainMaxLength)), MinValue(1)]
        public int constrainIterations = 1;

        [BoxGroup("Gravity")]
        public bool gravity;

        [BoxGroup("Gravity")]
        [ShowIf(nameof(gravity))]
        public float gravityMultiplier = 1f;

        private SpringNode[] _nodes;

        private struct SpringNode
        {
            public bool IsFixed;
            public Vector3 Position;
            public Vector3 Velocity;
        }

        private bool CheckLengthConstrainDistance(float v)
        {
            return v > segmentLength;
        }

        public IEnumerable<Vector3> Positions => _nodes.Select(node => node.Position);
        
        [DrawGizmo(GizmoType.NonSelected | GizmoType.Active)]
        private static void DrawNodeGizmos(SpringTentacle tentacle, GizmoType gizmoType)
        {
            if (tentacle._nodes == null) return;
            
            Gizmos.color = Color.yellow;
            foreach (var node in tentacle._nodes)
            {
                Gizmos.DrawSphere(node.Position, 0.1f);
            }
        }

        public Vector3 GetNodePosition(int nodeIndex)
        {
            return _nodes[nodeIndex].Position;
        }
        
        public void ApplyForceToNode(int nodeIndex, Vector3 force)
        {
            ref var node = ref _nodes[nodeIndex];
            node.Velocity += force / nodeMass * Time.fixedDeltaTime;
        }
        
        private void Awake()
        {
            CreateNodes();
            _nodes[0].IsFixed = true;
        }

        private void FixedUpdate()
        {
            _nodes[0].Position = transform.position;
            SimulateNodes(Time.deltaTime);
        }

        private void SimulateNodes(float deltaTime)
        {
            for (var i = 0; i < nodeCount; i++)
            {
                ref var currentNode = ref _nodes[i];

                if(currentNode.IsFixed) continue;

                if (gravity)
                {
                    SimulateGravity(ref currentNode, deltaTime);
                }

                if (simulateSprings && i >= 1)
                {
                    SimulateSpring(ref currentNode, _nodes[i-1], deltaTime);
                }
                
                MoveNode(ref currentNode, deltaTime);
            }

            if (constrainMaxLength)
            {
                SimulateNodeConstraints();
            }
        }

        private void SimulateSpring(ref SpringNode currentNode, SpringNode previousNode, float deltaTime)
        {
            var springAcceleration =
                CalculateSpringAcceleration(ref currentNode, previousNode.Position, k, dampening, deltaTime);
            currentNode.Velocity += springAcceleration;
        }
        
        private Vector3 CalculateSpringAcceleration(ref SpringNode currentNode, Vector3 towards, float springK, float damp, float deltaTime)
        {
            var toPrevious = towards - currentNode.Position;
            var distance = Mathf.Max(0.001f, toPrevious.magnitude);
            var normalizedToPrevious = toPrevious / distance;
            
            //f = m*a
            //a = f/m
            //f = -k*distance (already inverted on the vector)
            var force = springK * (distance - segmentLength);
            var forceVector = normalizedToPrevious * force;
            var dampForce = forceVector - currentNode.Velocity * damp;
            var acceleration = dampForce / nodeMass;
            return acceleration * deltaTime;
        }

        private void SimulateGravity(ref SpringNode node, float deltaTime)
        {
            node.Velocity += Physics.gravity * (deltaTime * gravityMultiplier);
        }

        private void SimulateNodeConstraints()
        {
            for (var j = 0; j < constrainIterations; j++)
            {
                for (var i = 0; i < nodeCount; i++)
                {
                    if(i == 0) continue;
                    ConstrainNodes(ref _nodes[i], ref _nodes[i-1]);
                }
            }
        }
        
        private void ConstrainNodes(ref SpringNode current, ref SpringNode previous)
        {
            if (current.IsFixed && previous.IsFixed)
            {
                return;
            }
            
            var toPrevious = previous.Position - current.Position;
            var distance = Mathf.Max(0.001f, toPrevious.magnitude);

            if (distance < maxLengthConstrainDistance)
            {
                return;
            }
            
            var normalizedToPrevious = toPrevious / distance;
            var exceedingDistance = distance - maxLengthConstrainDistance;

            if (previous.IsFixed)
            {
                current.Position += normalizedToPrevious * exceedingDistance;
                return;
            }

            if (current.IsFixed)
            {
                previous.Position += normalizedToPrevious * exceedingDistance;
                return;
            }
            
            current.Position += normalizedToPrevious * exceedingDistance * 0.5f;
            previous.Position += normalizedToPrevious * exceedingDistance * -0.5f;
        }

        private static void MoveNode(ref SpringNode node, float deltaTime)
        {
            node.Position += node.Velocity * deltaTime;
        }

        private void CreateNodes()
        {
            _nodes = new SpringNode[nodeCount];

            for (var i = 0; i < nodeCount; i++)
            {
                _nodes[i] = new SpringNode
                {
                    Position = transform.position + i * 0.1f * Vector3.down
                };
            }
        }
    }
}
