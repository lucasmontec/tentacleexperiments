using UnityEngine;

namespace TentacleSystems
{
    public class TransformTrackTentacleNode : MonoBehaviour
    {
        public SpringTentacle tentacle;

        public int nodeIndex;

        private void FixedUpdate()
        {
            transform.position = tentacle.GetNodePosition(nodeIndex);
        }
    }
}
