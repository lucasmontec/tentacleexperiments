using System.Collections.Generic;
using UnityEngine;

namespace TentacleSystems
{
    public interface INodePositionProvider
    {
        IEnumerable<Vector3> Positions { get; }
    }
}