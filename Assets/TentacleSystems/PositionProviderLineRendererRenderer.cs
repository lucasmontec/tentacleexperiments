using System;
using System.Linq;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

namespace TentacleSystems
{
    public class PositionProviderLineRendererRenderer : SerializedMonoBehaviour
    {
        [Required]
        public LineRenderer lineRenderer;
        
        [Required, NonSerialized, OdinSerialize]
        public INodePositionProvider PositionProvider;

        private void Start()
        {
            lineRenderer.positionCount = PositionProvider.Positions.Count();
        }

        private void FixedUpdate()
        {
            var positionIndex = 0;
            foreach (var position in PositionProvider.Positions)
            {
                lineRenderer.SetPosition(positionIndex, position);
                positionIndex++;
            }
        }
    }
}