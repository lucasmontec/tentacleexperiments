using Sirenix.OdinInspector;
using UnityEngine;

namespace TentacleSystems
{
    public class TentacleMouseTracker : SerializedMonoBehaviour
    {
        [Required]
        public SpringTentacle tentacle;

        public new Camera camera;

        public int nodeIndex;

        public float force;
        
        private void FixedUpdate()
        {
            if(!Input.GetMouseButton(0)) return;
            
            var mousePosition = camera.ScreenToWorldPoint(Input.mousePosition);

            var nodePosition = tentacle.GetNodePosition(nodeIndex);
            var toMouse = mousePosition - nodePosition;
            var distance = toMouse.magnitude;
            var normalizedToMouse = toMouse / distance;

            var forceVector = normalizedToMouse * force;
            tentacle.ApplyForceToNode(nodeIndex, forceVector);
        }
    }
}